package gof.Model;

import gof.Controller.Controller;
import javafx.animation.Animation;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Duration;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;

import static gof.stale.*;


import static javafx.animation.Animation.INDEFINITE;


/**
 * klasa do łatwej symulacji  gry w niegraficzny sposob
 * wstępnie reprezentujemy pixele na planszy jako klase pixeloza zawierajacą
 * macierz rozmiarX x rozmiarY pixseli. pixel po stworzeniu ma czas zycia ustawiony na domyslna wartosc z klasy stale
 * kazde wywolanie plynieCzas () iteruje po calej planszy i 1) zmniejsza czas zycia kazdej zyjacej komorki o jeden
 * 2)ustawia czas zycia na -1 dla komorek ktore umieraja z przeludnienia
 * 3)generuje nowe komorki o czasie zycia=stale.domyslnyCzasZycia w miejscach ktore granicza z odpowiednia liczba komorek
 */


public class pixeloza {

    public int rozmiarX;
    public int rozmiarY;
    public double speed;

    private Timeline timeline;
    private EventHandler<ActionEvent> eventHandler;
    private Set<Integer> przezywanie;
    private Set<Integer> powstawanie;


    public pixel[][] plansza;

    public void setSpeed(double v) {
        speed = v;
        Animation.Status status = timeline.getStatus();
        timeline.stop();
        KeyFrame keyFrame = new KeyFrame(new Duration(speed), eventHandler);
        timeline = new Timeline(keyFrame);
        timeline.setCycleCount(INDEFINITE);
        if (status == Animation.Status.RUNNING) {
            timeline.play();
        } else if (status == Animation.Status.PAUSED) {
            timeline.pause();
        } else {
            timeline.stop();
        }
    }

    public class pixel {

        Controller.PixelPane gPixel;
        boolean exists;
        boolean existed;
        int czasZycia;

        /**
         * współrzędne piksela na planszy
         */
        public int x;
        public int y;


        //long czasZycia;

        /**
         * domyslne pixele maja czas zycia=domyslny czas zycia ktory zmniejszamy o jeden w kazdym wywolaniu symulujZycie
         * existance oznacza czy żyje
         */
        pixel() {
            czasZycia = 0;
            exists = false;
            existed = false;
        }


        pixel(int x, int y) {
            this();
            this.x = x;
            this.y = y;
        }


        void symulujZycie(int x, int y) {

            int zyjacySasiedzi = 0;

            if (wasAlive(x - 1, y - 1)) zyjacySasiedzi++;
            if (wasAlive(x, y - 1)) zyjacySasiedzi++;
            if (wasAlive(x + 1, y - 1)) zyjacySasiedzi++;
            if (wasAlive(x - 1, y)) zyjacySasiedzi++;
            if (wasAlive(x + 1, y)) zyjacySasiedzi++;
            if (wasAlive(x - 1, y + 1)) zyjacySasiedzi++;
            if (wasAlive(x, y + 1)) zyjacySasiedzi++;
            if (wasAlive(x + 1, y + 1)) zyjacySasiedzi++;

            if (!existed) {
                if (powstawanie.contains(zyjacySasiedzi)) poczatek();
                else koniec();
            } else {
                if (!przezywanie.contains(zyjacySasiedzi)) koniec();
                //poczatek();
                //else koniec();
            }

            if (exists) czasZycia++;
            if (czasZycia == 4) gPixel.setAliveForLong();
            if (czasZycia == 8) gPixel.setAliveForLongest();
        }

        /**
         * zwracamy true/false w zaleznosci czy komorka bedzie dalej zyc
         */
        public boolean zyje() {
            return (exists);
        }

        /**
         * komorka umiera
         */
        public void koniec() {
            czasZycia = 0;
            exists = false;
            gPixel.setAlive(false);
        }

        public void poczatek() {
            czasZycia = 0;
            exists = true;
            gPixel.setAlive(true);
        }

        public void setgPixel(Controller.PixelPane p) {
            gPixel = p;
        }
    }

    private boolean wasAlive(int x, int y) {
        x = x % rozmiarY;
        y = y % rozmiarX;
        if (x < 0) x += rozmiarY;
        if (y < 0) y += rozmiarX;
        return plansza[x][y].existed;
    }

    /**
     * konstruuje prostokątną plansze
     */
    public pixeloza(int rozmiarX, int rozmiarY) {
        this.rozmiarX = rozmiarX;
        this.rozmiarY = rozmiarY;
        speed = initialSpeed;
        przezywanie = new HashSet<>(Arrays.asList(2, 3));
        powstawanie = new HashSet<>(Arrays.asList(3));

        plansza = new pixel[rozmiarY][rozmiarX];
        for (int i = 0; i < rozmiarY; i++) {
            for (int j = 0; j < rozmiarX; j++) {
                plansza[i][j] = new pixel(j, i);
            }
        }
        updateTimeline();
    }

    /**
     * konstruuje kwadratową plansze
     */
    public pixeloza(int rozmiar) {
        this(rozmiar, rozmiar);
    }

    private void updateTimeline() {
        eventHandler = getEventHandler();
        KeyFrame keyFrame = new KeyFrame(new Duration(speed), eventHandler);
        timeline = new Timeline(keyFrame);
        timeline.setCycleCount(INDEFINITE);
    }

    private EventHandler<ActionEvent> getEventHandler() {
        return event -> next();
    }

    private void next() {
        plynieCzas();
    }


    /**
     * iterujemy po calej planszy, i dla kazdej komorki wywolujemy symulujZycie() ktore ustawia wartość posostalego czasu zycia komorek na wartosc
     * z zakresu -1 - domyslnyCzasZycia
     */
    private void plynieCzas() {
        for (int i = 0; i < rozmiarY; i++) {
            for (int j = 0; j < rozmiarX; j++) {
                plansza[i][j].existed = plansza[i][j].exists;
            }
        }
        for (int i = 0; i < rozmiarY; i++) {
            for (int j = 0; j < rozmiarX; j++) {
                plansza[i][j].symulujZycie(i, j);
            }
        }

    }


    public void play() {
        timeline.play();
    }


    public void pause() {
        timeline.pause();
    }

    public void losuj() {
        Random rand = new Random();
        for (int i = 0; i < rozmiarY; i++) {
            for (int j = 0; j < rozmiarX; j++) {
                if (!plansza[i][j].zyje() && rand.nextInt() % 10 == 0) plansza[i][j].poczatek();
            }
        }
    }

    public void clear() {
        for (int i = 0; i < rozmiarY; i++) {
            for (int j = 0; j < rozmiarX; j++) {
                plansza[i][j].koniec();
            }
        }
    }

    public String save() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < rozmiarY; i++) {
            for (int j = 0; j < rozmiarX; j++) {
                if (!plansza[i][j].zyje()) {
                    sb.append('0');
                    continue;
                }
                if (plansza[i][j].czasZycia < 8) {
                    sb.append(plansza[i][j].czasZycia);
                    continue;
                }
                sb.append('9'); //komórka czerwona
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public void read(String s) {
        int poz = 0;
        for (int i = 0; i < rozmiarY; i++) {
            for (int j = 0; j < rozmiarX; j++) {
                if (s.charAt(poz) == '0') {
                    plansza[i][j].koniec();
                } else {
                    plansza[i][j].poczatek();
                    plansza[i][j].czasZycia = Character.getNumericValue(s.charAt(poz));
                    if (plansza[i][j].czasZycia >= 4) {
                        if (plansza[i][j].czasZycia < 8) plansza[i][j].gPixel.setAliveForLong();
                        else plansza[i][j].gPixel.setAliveForLongest();
                    }
                }
                ++poz;
            }
            //++poz;
        }
    }

    /**
     * Funkcja zmieniająca liczby sąsiadów potrzebnych do przeżycia danej komórki
     *
     * @param hs nowy zbiór liczb oznaczających, ilu sąsiadów potrzebuje komórka, aby przeżyć
     */
    public void zmienPrzezywanie(HashSet<Integer> hs) {
        przezywanie = hs;
    }


    /**
     * Funkcja zmieniająca liczby sąsiadów potrzebnych do narodzenia się danej komórki
     *
     * @param hs nowy zbiór liczb oznaczających, ilu sąsiadów potrzebuje komórka, aby się narodzić
     */
    public void zmienPowstawanie(HashSet<Integer> hs) {
        powstawanie = hs;
    }
}
