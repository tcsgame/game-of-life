package gof.Controller;

import gof.Main;
import gof.Model.pixeloza;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.beans.EventHandler;
import java.io.*;
import java.text.NumberFormat;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static gof.stale.*;


public class Controller {
    @FXML
    Button clear;

    @FXML
    Button save;

    @FXML
    Button load;

    @FXML
    Button start;

    @FXML
    Button los;

    @FXML
    Slider sizeSlider;

    @FXML
    Slider speedSlide;

    @FXML
    Button figury;

    @FXML
    TextField przezywanie;

    @FXML
    TextField powstawanie;

    @FXML
    Label errorText;

    /**
     * zmienna przechowująca graficzną reprezentację głównej planszy
     */
    private GridPane board;

    /**
     * Flaga informująca planszę, czy właśnie jesteśmy w trakcie przeciągania jakiegoś obiektu
     */
    static boolean dragFlag = false;
    /**
     * Lista przechowująca dane o przeciąganym aktualnie obiekcie (jeżeli taki istnieje)
     */
    static List<Pair<Integer, Integer>> dragList = null;


    private pixeloza gameBoard; //silnik głównej planszy
    private boolean started; //czy gra została rozpoczęta
    private boolean isPaused; //czy gra jest zapauzowana
    private double scale; //aktualna skala przybliżenia


    /**
     * klasa reprezentująca pojedynczy graficzny punkt na planszy
     */
    public static class PixelPane {
        private pixeloza.pixel p; //piksel obserwowany przez klasę

        private Pane zywaKrotko;
        private Pane zywaDluzej;
        private Pane zywaNajdluzej;
        private Pane martwa;

        PixelPane(pixeloza.pixel p) {
            this.p = p;
            zywaKrotko = new Pane();
            zywaDluzej = new Pane();
            zywaNajdluzej = new Pane();
            martwa = new Pane();
            zywaKrotko.setStyle("-fx-background-color:" + green);
            zywaDluzej.setStyle("-fx-background-color:" + yellow);
            zywaNajdluzej.setStyle("-fx-background-color:" + red);
            martwa.setStyle("-fx-background-color:" + grey);
        }

        pixeloza.pixel getPixel() {
            return p;
        }

        Pane getZywaKrotko() {
            return zywaKrotko;
        }

        Pane getMartwa() {
            return martwa;
        }

        Pane getZywaDluzej() { return zywaDluzej; }

        Pane getZywaNajdluzej() { return zywaNajdluzej; }


        public void setAlive(boolean flag) {
            if (flag) {
                zywaKrotko.setVisible(true);
                martwa.setVisible(false);
                zywaDluzej.setVisible(false);
                zywaNajdluzej.setVisible(false);
            } else {
                martwa.setVisible(true);
                zywaKrotko.setVisible(false);
                zywaDluzej.setVisible(false);
                zywaNajdluzej.setVisible(false);
            }
        }

        public void setAliveForLongest() {
            zywaNajdluzej.setVisible(true);
            zywaDluzej.setVisible(false);
            zywaKrotko.setVisible(false);
            martwa.setVisible(false);
        }

        public void setAliveForLong() {
            zywaDluzej.setVisible(true);
            zywaNajdluzej.setVisible(false);
            zywaKrotko.setVisible(false);
            martwa.setVisible(false);
        }
    }


    /**
     * Funkcja inicjalizująca elementy głównej planszy
     */
    @FXML
    public void initialize() {
        sizeSlider.setValue(0);
        initBoard((AnchorPane) start.getParent(), sizeSlider.getValue());
        started = false;
        scale = sizeSlider.getValue();


        sizeSlider.valueProperty().addListener((ObservableValue<? extends Number> ov,  //przybliżanie/oddalanie
                                                Number old_val, Number new_val) ->
                setscale((double) new_val));

        speedSlide.valueProperty().addListener((ObservableValue<? extends Number> ov,
                                                Number old_val, Number new_val) ->
                gameBoard.setSpeed(-(double) new_val));
        // gameBoard.speed = -(double) new_val);

        board.setOnScroll(ScrollEvent -> {
            double newScale = scale - ScrollEvent.getDeltaY();
            if (newScale > sizeSlider.getMax()) newScale = sizeSlider.getMax();
            else if (newScale < sizeSlider.getMin()) newScale = sizeSlider.getMin();
            setscale(newScale);
            sizeSlider.setValue(newScale);
        });

        przezywanie.setText("23");
        powstawanie.setText("3");

      /*  przezywanie.setOnAction(EventHandler -> {
            gameBoard.pause();
            start.setText("Start");
            started = false;
            isPaused = true;
            HashSet<Integer> newSet = newRulesSet(przezywanie);
            if (newSet == null) {
                errorText.setVisible(true);
            }
            gameBoard.zmienPrzezywanie(newSet);
        });*/

    /*    powstawanie.setOnAction(EventHandler -> {
            gameBoard.pause();
            start.setText("Start");
            started = false;
            isPaused = true;
            HashSet<Integer> newSet = newRulesSet(powstawanie);
            if (newSet == null) {
                errorText.setVisible(true);
            }
            gameBoard.zmienPowstawanie(newSet);
        });*/

        initTextListener(powstawanie);
        initTextListener(przezywanie);

    }


    /**
     * Funkcja inicjalizująca zachowanie przycisku Start/Stop
     */
    @FXML
    public void startInit() {
        start.setOnMouseClicked(MouseEvent -> {
            if (!started) {

                HashSet<Integer> newSet = newRulesSet(powstawanie);
                if (newSet == null) {
                    errorText.setVisible(true);
                    return;
                }
                gameBoard.zmienPowstawanie(newSet);

                newSet = newRulesSet(przezywanie);
                if (newSet == null) {
                    errorText.setVisible(true);
                    return;
                }
                gameBoard.zmienPrzezywanie(newSet);

                errorText.setVisible(false);
                start.setText("Stop");
                gameBoard.play();
                started = true;
                isPaused = false;
            } else {
                if (isPaused) {
                    HashSet<Integer> newSet = newRulesSet(powstawanie);
                    if (newSet == null) {
                        errorText.setVisible(true);
                        return;
                    }
                    gameBoard.zmienPowstawanie(newSet);

                    newSet = newRulesSet(przezywanie);
                    if (newSet == null) {
                        errorText.setVisible(true);
                        return;
                    }
                    gameBoard.zmienPrzezywanie(newSet);

                    errorText.setVisible(false);
                    start.setText("Stop");
                    gameBoard.play();
                } else {
                    start.setText("Start");
                    gameBoard.pause();
                }
                isPaused = !isPaused;
            }
        });
    }

    @FXML
    public void losuj() {
        los.setOnMouseClicked(MouseEvent -> gameBoard.losuj());
    }

    @FXML
    public void clear() {
        clear.setOnMouseClicked(MouseEvent -> gameBoard.clear());
    }

    @FXML
    public void zapisz() {
        FileChooser fileChooser = new FileChooser();

        File file = fileChooser.showSaveDialog(null);

        if(file != null){
            try {
                FileWriter fileWriter;
                fileWriter = new FileWriter(file);
                fileWriter.write(gameBoard.save());
                fileWriter.close();
            } catch (IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }


    @FXML
    public void wczytaj() {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(null);
        if(file != null){
            gameBoard.read(readFile(file));
        }
    }

    private String readFile(File file){
        StringBuilder stringBuffer = new StringBuilder();
        BufferedReader bufferedReader = null;

        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            String text;
            while ((text = bufferedReader.readLine()) != null) {
                stringBuffer.append(text);
            }
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return stringBuffer.toString();
    }


    @FXML
    public void setFigury() {
        figury.setOnMouseClicked(MouseEvent -> {
            try {
                Stage stage = new Stage();
                TabPane rootLayout;
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(Main.class.getResource("View/figury.fxml"));
                rootLayout = loader.load();
                Scene s = new Scene(rootLayout);
                stage.setScene(s);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }


    private void initBoard(AnchorPane root, double initScale) {
        board = new GridPane();
        gameBoard = new pixeloza(columns, rows);

        /*
         * poszerzanie GridPane
         */
        for (int i = 0; i < columns; i++) {
            ColumnConstraints colConst = new ColumnConstraints();
            colConst.setPercentWidth(100.0 / columns);
            board.getColumnConstraints().add(colConst);
        }
        for (int i = 0; i < rows; i++) {
            RowConstraints rowConst = new RowConstraints();
            rowConst.setPercentHeight(100.0 / rows);
            board.getRowConstraints().add(rowConst);
        }


        /*
         * dodawanie do każdej komórki board kolejno PixelPane'ów
         */
        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                PixelPane tmp = new PixelPane(gameBoard.plansza[j][i]);

                setOnMouseEvent(tmp, tmp.zywaNajdluzej);
                setOnMouseEvent(tmp, tmp.martwa);
                setOnMouseEvent(tmp, tmp.zywaDluzej);
                setOnMouseEvent(tmp, tmp.zywaKrotko);

                gameBoard.plansza[j][i].setgPixel(tmp);
                tmp.martwa.setVisible(true);
                tmp.zywaKrotko.setVisible(false);
                tmp.zywaDluzej.setVisible(false);
                tmp.zywaNajdluzej.setVisible(false);
                board.add(tmp.zywaKrotko, i, j);
                board.add(tmp.martwa, i, j);
                board.add(tmp.zywaDluzej, i, j);
                board.add(tmp.zywaNajdluzej, i, j);
            }
        }

        //board.setGridLinesVisible(true);
        root.getChildren().add(board);

        /*
         * dopasowanie board do okna
         */
        AnchorPane.setTopAnchor(board, initScale);
        AnchorPane.setLeftAnchor(board, initScale);
        AnchorPane.setRightAnchor(board, initScale);
        AnchorPane.setBottomAnchor(board, 90.0);
    }


    /**
     * funkcja zmieniająca przybliżenie
     */
    private void setscale(double s) {
        AnchorPane.setTopAnchor(board, s);
        AnchorPane.setLeftAnchor(board, s);
        AnchorPane.setRightAnchor(board, s);
        AnchorPane.setBottomAnchor(board, 90.0);
        scale = s;
    }

    private void setOnMouseEvent(PixelPane pixelPane, Pane pane) {

        pane.setOnMousePressed(MouseEvent -> {
            if (dragFlag) {
                int x = pixelPane.p.x;
                int y = pixelPane.p.y;
                for (Pair<Integer, Integer> p : dragList) {
                    if (y + p.getKey() < gameBoard.rozmiarY && x + p.getValue() < gameBoard.rozmiarX
                            && y + p.getKey() >= 0 && x + p.getValue() >= 0)
                        gameBoard.plansza[y + p.getKey()][x + p.getValue()].poczatek();
                }
                dragList = null;
                dragFlag = false;
                if (!pixelPane.p.zyje())
                    pixelPane.martwa.setStyle("-fx-background-color: " + grey);
            } else {
                if (pixelPane.getPixel().zyje()) {
                    pixelPane.getPixel().koniec();
                } else {
                    pixelPane.getPixel().poczatek();
                }
            }

        });
    }

    /*
     * Pomocnicza funkcja odczytująca nowe zasady do zbioru. Jeżeli operacja sie nie powiedzie, funkcja zwraca null
     */
    private HashSet<Integer> newRulesSet(TextField field) {
        gameBoard.pause();
        String val = field.getText();
        if (val.length() > 9) {
            errorText.setVisible(true);
            return null;
        }
        HashSet<Integer> rules = new HashSet<>();
        for (int i = 0; i < val.length(); i++) {
            int v = Character.getNumericValue(val.charAt(i));
            if (v < 0 || v > 8) return null;
            rules.add(v);
        }
        errorText.setVisible(false);
        return rules;
    }

    /*
     * Funkcja ustawiająca pola służące do zmiany zasad gry
     */
    private void initTextListener(TextField tf) {
        tf.textProperty().addListener((observable, oldValue, newValue) -> {
            gameBoard.pause();
            start.setText("Start");
            started = false;
            isPaused = true;
        });
    }


}
