package gof.Controller;

import gof.Model.pixeloza;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

import static gof.stale.*;

public class ControllerFigury {

    /**
     * Klasa pomocnicza, przechowująca wszystkie obiekty potrzebne do zarządzania mini-planszami w oknie z figurami
     */

    public class GameboardGridPane extends GridPane {

        private GridPane graphicalBoard;
        private pixeloza gameBoard;
        private AnchorPane boardPane;
        private boolean started; //czy gra została rozpoczęta
        private final int sizeX;
        private final int sizeY;
        private List<Pair<Integer, Integer>> shape;


        GameboardGridPane(GridPane graphicalBoard, AnchorPane boardPane,
                                 pixeloza gameBoard, int size, List<Pair<Integer, Integer>> shape) {
            this(graphicalBoard, boardPane, gameBoard, size, size, shape);
        }

        /**
         * @param graphicalBoard klasa przechowująca graficzną reprezentację mini-planszy
         * @param boardPane AnchorPane, w którym znajduje sie grafika; pole służące do obsługi zachowania myszy
         * @param gameBoard silnik mini-planszy
         * @param sizeX rozmiar planszy (współrzędna X)
         * @param sizeY rozmiar planszy (współrzędna Y)
         * @param shape kształt figury; bloki powinny być posortowane leksykograficznie
         */

        GameboardGridPane(GridPane graphicalBoard, AnchorPane boardPane,
                                 pixeloza gameBoard, int sizeX, int sizeY, List<Pair<Integer, Integer>> shape) {
            this.sizeX = sizeX;
            this.sizeY = sizeY;
            this.graphicalBoard = graphicalBoard;
            this.gameBoard = gameBoard;
            this.boardPane = boardPane;
            this.shape = new ArrayList<>();
            this.shape.addAll(shape);
            gameBoard.setSpeed(100);
        }


        int sizeX() {
            return sizeX;
        }

        int sizeY() { return sizeY; }

        GridPane getGridPane() {
            return graphicalBoard;
        }

        pixeloza getGameBoard() {
            return gameBoard;
        }
    }


    /*
     * Mini-plansze używane do wyświetlania zachowania
     * poszczególnych figur, ich reprezentacje graficzne, oraz klasy skupiające wszystkie dane o nich
     */

    @FXML
    GridPane blinker;
    @FXML
    AnchorPane blinkerPane;

    @FXML
    GridPane zabka;
    @FXML
    AnchorPane zabkaPane;

    @FXML
    GridPane szybowiec;
    @FXML
    AnchorPane szybowiecPane;

    @FXML
    GridPane dakota;
    @FXML
    AnchorPane dakotaPane;

    @FXML
    GridPane beacon;
    @FXML
    AnchorPane beaconPane;

    @FXML
    GridPane krokodyl;
    @FXML
    AnchorPane krokodylPane;

    @FXML
    GridPane fontanna;
    @FXML
    AnchorPane fontannaPane;

    @FXML
    GridPane gilderGun;
    @FXML
    AnchorPane gilderGunPane;

    @FXML
    GridPane HWSS;
    @FXML
    AnchorPane HWSSPane;


    /**
     * Funkcja inicjująca mini-planszę daną jako argument targetPane,
     * @param speed prędkość, z jaką będą rozwijały się struktury; domyślnie speed = 500
     * @param targetPane obiekt przechowujący dane o kształcie, który dodajemy
     */
    private void initBoard(GameboardGridPane targetPane, double speed) {
        Queue<Pair<Integer, Integer>> pola = new LinkedBlockingDeque<>();
        pola.addAll(targetPane.shape);
        int middleX = targetPane.sizeX / 2;
        if(targetPane.sizeX % 2 == 0)
            middleX--;
        int middleY = targetPane.sizeY / 2;
        if(targetPane.sizeY % 2 == 0)
            middleY--;
        Pair<Integer, Integer> p = pola.element();
        pola.remove();
        for (int i = 0; i < targetPane.sizeY(); i++) {
            for (int j = 0; j < targetPane.sizeX(); j++) {
                Controller.PixelPane tmp = new Controller.PixelPane(targetPane.getGameBoard().plansza[j][i]);

                targetPane.getGameBoard().plansza[j][i].setgPixel(tmp);
                targetPane.getGridPane().add(tmp.getMartwa(), i, j);
                targetPane.getGridPane().add(tmp.getZywaKrotko(), i, j);
                targetPane.getGridPane().add(tmp.getZywaDluzej(), i, j);
                targetPane.getGridPane().add(tmp.getZywaNajdluzej(), i, j);

                if(p != null && p.getKey() + middleY == i && p.getValue() + middleX == j) {
                    tmp.getPixel().poczatek();
                    if(pola.size() > 0) {
                        p = pola.element();
                        pola.remove();
                    }
                    else p = null;
                }
                else tmp.getPixel().koniec();

            }
        }
        initMouse(targetPane, speed);
    }

    private void initBoard(GameboardGridPane targetPane) {
        initBoard(targetPane, 500);
    }


    /**
     * funkcje konfigurujące zachowanie mini-plansz
     * do listy S dodajemy kolejno punkty, które mają tworzyć naszą strukturę
     * punkt (0, 0) traktujemy jako jej środek
     */
    @FXML
    void initialize() {
        //CYKLE

        //Blinker
        List<Pair<Integer, Integer>> S = new ArrayList<>();
        S.add(new Pair<>(0, -1));
        S.add(new Pair<>(0, 0));
        S.add(new Pair<>(0, 1));
        GameboardGridPane blinkerBoard = new GameboardGridPane(blinker, blinkerPane,
                new pixeloza(blinkerSize), blinkerSize, S);
        initBoard(blinkerBoard);

        //Żabka
        S = new ArrayList<>();
        S.add(new Pair<>(-1, 0));
        S.add(new Pair<>(-1, 1));
        S.add(new Pair<>(0, -1));
        S.add(new Pair<>(1, 2));
        S.add(new Pair<>(2, 0));
        S.add(new Pair<>(2, 1));
        GameboardGridPane zabkaBoard = new GameboardGridPane(zabka, zabkaPane,
                new pixeloza(zabkaSize), zabkaSize, S);
        initBoard(zabkaBoard);

        //Latarnia morska
        S = new ArrayList<>();
        S.add(new Pair<>(-1, -1));
        S.add(new Pair<>(-1, 0));
        S.add(new Pair<>(0, -1));
        S.add(new Pair<>(1, 2));
        S.add(new Pair<>(2, 1));
        S.add(new Pair<>(2, 2));
        GameboardGridPane beaconBoard = new GameboardGridPane(beacon, beaconPane,
                new pixeloza(beaconSize), beaconSize, S);
        initBoard(beaconBoard);

        //Krokodyl
        S = new ArrayList<>();
        S.add(new Pair<>(-1, -2));
        S.add(new Pair<>(-1, 3));
        S.add(new Pair<>(0, -4));
        S.add(new Pair<>(0, -3));
        S.add(new Pair<>(0, -1));
        S.add(new Pair<>(0, 0));
        S.add(new Pair<>(0, 1));
        S.add(new Pair<>(0, 2));
        S.add(new Pair<>(0, 4));
        S.add(new Pair<>(0, 5));
        S.add(new Pair<>(1, -2));
        S.add(new Pair<>(1, 3));
        GameboardGridPane krokodylBoard = new GameboardGridPane(krokodyl, krokodylPane,
                new pixeloza(krokodylSizeCols, krokodylSizeRows), krokodylSizeRows, krokodylSizeCols, S);
        initBoard(krokodylBoard);

        //Fontanna
        S = new ArrayList<>();
        S.add(new Pair<>(-4, -2));
        S.add(new Pair<>(-4, -1));
        S.add(new Pair<>(-3, -2));
        S.add(new Pair<>(-3, -1));
        S.add(new Pair<>(-2, -2));
        S.add(new Pair<>(-2, 2));
        S.add(new Pair<>(-1, -1));
        S.add(new Pair<>(-1, 0));
        S.add(new Pair<>(-1, 2));
        S.add(new Pair<>(1, -1));
        S.add(new Pair<>(1, 0));
        S.add(new Pair<>(1, 2));
        S.add(new Pair<>(2, -2));
        S.add(new Pair<>(2, 2));
        S.add(new Pair<>(3, -2));
        S.add(new Pair<>(3, -1));
        S.add(new Pair<>(4, -2));
        S.add(new Pair<>(4, -1));
        GameboardGridPane fontannaBoard = new GameboardGridPane(fontanna, fontannaPane,
                new pixeloza(fontannaSizeCols, fontannaSizeRows), fontannaSizeRows, fontannaSizeCols, S);
        initBoard(fontannaBoard);

        //SZYBOWCE

        //szybowiec
        S = new ArrayList<>();
        S.add(new Pair<>(-1, 0));
        S.add(new Pair<>(0, 1));
        S.add(new Pair<>(1, -1));
        S.add(new Pair<>(1, 0));
        S.add(new Pair<>(1, 1));
        GameboardGridPane szybowiecBoard = new GameboardGridPane(szybowiec, szybowiecPane,
                new pixeloza(szybowiecSize), szybowiecSize, S);
        initBoard(szybowiecBoard);

        //dakota
        S = new ArrayList<>();
        S.add(new Pair<>(-2, -1));
        S.add(new Pair<>(-2, 0));
        S.add(new Pair<>(-2, 1));
        S.add(new Pair<>(-1, -2));
        S.add(new Pair<>(-1, 1));
        S.add(new Pair<>(0, 1));
        S.add(new Pair<>(1, 1));
        S.add(new Pair<>(2, -2));
        S.add(new Pair<>(2, 0));
        GameboardGridPane dakotaBoard = new GameboardGridPane(dakota, dakotaPane,
                new pixeloza(dakotaSizeCols, dakotaSizeRows), dakotaSizeRows, dakotaSizeCols, S);
        initBoard(dakotaBoard);

        //ciężki statek kosmiczny
        S = new ArrayList<>();
        S.add(new Pair<>(-3, -1));
        S.add(new Pair<>(-3, 0));
        S.add(new Pair<>(-3, 1));
        S.add(new Pair<>(-2, -2));
        S.add(new Pair<>(-2, 1));
        S.add(new Pair<>(-1, 1));
        S.add(new Pair<>(0, -3));
        S.add(new Pair<>(0, 1));
        S.add(new Pair<>(1, -3));
        S.add(new Pair<>(1, 1));
        S.add(new Pair<>(2, 1));
        S.add(new Pair<>(3, -2));
        S.add(new Pair<>(3, 0));
        GameboardGridPane HWSSBoard = new GameboardGridPane(HWSS, HWSSPane,
                new pixeloza(HWSSSizeCols, HWSSSizeRows), HWSSSizeRows, HWSSSizeCols, S);
        initBoard(HWSSBoard);


        //GILDER GUN
        S = new ArrayList<>();
        S.add(new Pair<>(5, -5));
        S.add(new Pair<>(5, -4));
        S.add(new Pair<>(6, -6));
        S.add(new Pair<>(6, -2));
        S.add(new Pair<>(7,-7));
        S.add(new Pair<>(7, -1));
        S.add(new Pair<>(7, 7));
        S.add(new Pair<>(8, -17));
        S.add(new Pair<>(8, -16));
        S.add(new Pair<>(8, -7));
        S.add(new Pair<>(8, -3));
        S.add(new Pair<>(8, -1));
        S.add(new Pair<>(8, 0));
        S.add(new Pair<>(8, 5));
        S.add(new Pair<>(8, 7));
        S.add(new Pair<>(9, -17));
        S.add(new Pair<>(9, -16));
        S.add(new Pair<>(9, -7));
        S.add(new Pair<>(9, -1));
        S.add(new Pair<>(9, 3));
        S.add(new Pair<>(9, 4));
        S.add(new Pair<>(10, -6));
        S.add(new Pair<>(10, -2));
        S.add(new Pair<>(10, 3));
        S.add(new Pair<>(10, 4));
        S.add(new Pair<>(10, 17));
        S.add(new Pair<>(10, 18));
        S.add(new Pair<>(11, -5));
        S.add(new Pair<>(11, -4));
        S.add(new Pair<>(11, 3));
        S.add(new Pair<>(11, 4));
        S.add(new Pair<>(11, 17));
        S.add(new Pair<>(11, 18));
        S.add(new Pair<>(12, 5));
        S.add(new Pair<>(12, 7));
        S.add(new Pair<>(13, 7));
        GameboardGridPane gilderGunBoard = new GameboardGridPane(gilderGun, gilderGunPane,
                new pixeloza(gilderGunSizeCols, gilderGunSizeRows), gilderGunSizeRows, gilderGunSizeCols, S);
        initBoard(gilderGunBoard, 100);
    }

    /**
     * Funkcja ustawiająca zachowanie danego panelu, przechowującego grafikę kształtu, na poszczególne działania myszy
     * @param gp obiekt przechowujący informacje o kształcie
     * @param speed prędkość, z jaką rozwijają się struktury
     */
    private void initMouse(GameboardGridPane gp, double speed) {
        gp.boardPane.setOnMouseEntered(MouseEvent -> {
            if(!gp.started) {
                gp.gameBoard.play();
                gp.started = true;
                gp.gameBoard.speed = speed;
            }
        });

        gp.boardPane.setOnMouseExited(MouseEvent -> {
            if(gp.started) {
                gp.started = false;
                gp.gameBoard.pause();
            }
        });

        gp.boardPane.setOnMousePressed(MouseEvent -> {
            Controller.dragFlag = !Controller.dragFlag;
            if(Controller.dragList == null) {
                Controller.dragList = new ArrayList<>();
                Controller.dragList.addAll(gp.shape);
            }
            else Controller.dragList = null;
        });
    }
}
