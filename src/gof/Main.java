package gof;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.stage.Stage;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setTitle("Gra w życie");
        AnchorPane rootLayout;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("View/gof.fxml"));
        rootLayout = loader.load();
        Scene s = new Scene(rootLayout);
        primaryStage.setScene(s);
        //primaryStage.setResizable(false);
        primaryStage.show();
    }
}
