package gof;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class stale {
    public static int initialSpeed = 1000;

    public static int columns = 160;       //160
    public static int rows = 90;           //90

    public static final int blinkerSize = 5;
    public static final int zabkaSize = 6;
    public static final int szybowiecSize = 5;
    public static final int beaconSize = 6;
    public static final int krokodylSizeCols = 11;
    public static final int krokodylSizeRows = 18;
    public static final int fontannaSizeCols = 11;
    public static final int fontannaSizeRows = 9;
    public static final int gilderGunSizeCols = 11 + 19; //11 oryginalnie, ale dodaję 19 dla lepszego zobrazowania struktury
    public static final int gilderGunSizeRows = 38;
    public static final int dakotaSizeRows = 7;
    public static final int dakotaSizeCols = 20;
    public static final int HWSSSizeRows = 7;
    public static final int HWSSSizeCols = 20;



    public static String green = "#00fd00";
    public static String red = "#ff0000";
    public static String grey = "#a1a1a1";
    public static String white = "#ffffff";
    public static String yellow = "#fdff00";
    public static String highlightedGrey = "#737373";
    public static String highlightedYellow = "#ffffb3";
}
